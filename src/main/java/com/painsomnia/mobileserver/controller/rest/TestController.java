package com.painsomnia.mobileserver.controller.rest;

import com.painsomnia.mobileserver.bean.Member;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author: Sergey Vasilenko
 * Created: 14.10.16.
 */
@RestController
@RequestMapping("/api/test")
public class TestController {

    @RequestMapping(value = "/insecure", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Object index() {
        return new Member("zxc", "pwd");
    }

    @RequestMapping(value = "/secured", produces = {MediaType.APPLICATION_XML_VALUE})
    public Object indexSecured() {
        return new Member("sec", "ured");
    }
}
