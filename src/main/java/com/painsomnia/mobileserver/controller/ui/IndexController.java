package com.painsomnia.mobileserver.controller.ui;

import com.painsomnia.mobileserver.model.User;
import com.painsomnia.mobileserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Author: Sergey Vasilenko
 * Created: 14.10.16.
 */
@Controller
@RequestMapping("/ui")
public class IndexController {

    private final UserService userService;

    @Autowired
    public IndexController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = {"", "/index"})
    public String index() {
        return "index";
    }

    @RequestMapping("/secured")
    public String secured() {
        return "secured";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping("/users")
    public String users(@RequestParam(name = "q", required = false) String query, ModelMap modelMap) {
        final Iterable<User> users;
        if (query != null) {
            users = userService.searchUsers(query);
        } else {
            users = userService.findAll();
        }
        modelMap.addAttribute("users", users);
        return "users";
    }
}
