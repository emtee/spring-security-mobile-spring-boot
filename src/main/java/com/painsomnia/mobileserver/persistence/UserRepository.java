package com.painsomnia.mobileserver.persistence;

import com.painsomnia.mobileserver.constant.RepositoryQuery;
import com.painsomnia.mobileserver.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author: Sergey Vasilenko
 * Created: 17.10.16.
 */
@Repository
public interface UserRepository extends CrudRepository<User, String> {
    @Query(value = "select u " + RepositoryQuery.SEARCH_BY_MASK_QUERY,
            countQuery = "select count(u) " + RepositoryQuery.SEARCH_BY_MASK_QUERY)
    List<User> searchUsers(@Param("searchTerm") String searchTerm);
}
