package com.painsomnia.mobileserver.bean;

/**
 * Author: Sergey Vasilenko
 * Created: 14.10.16.
 */
public class Member {
    private String username;
    private String password;

    public Member() {
    }

    public Member(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
