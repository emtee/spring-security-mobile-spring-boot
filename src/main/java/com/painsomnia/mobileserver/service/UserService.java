package com.painsomnia.mobileserver.service;

import com.painsomnia.mobileserver.constant.Authority;
import com.painsomnia.mobileserver.model.User;

import java.util.List;

/**
 * Author: Sergey Vasilenko
 * Created: 17.10.16.
 */
public interface UserService {
    Iterable<User> findAll();

    User get(String username);

    User create(User user, Authority... authorities);

    User update(User user);

    boolean exists(String username);

    long count();

    List<User> searchUsers(String searchTerm);
}
