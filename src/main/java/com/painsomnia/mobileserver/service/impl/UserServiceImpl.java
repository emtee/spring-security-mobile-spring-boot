package com.painsomnia.mobileserver.service.impl;

import com.painsomnia.mobileserver.constant.Authority;
import com.painsomnia.mobileserver.model.User;
import com.painsomnia.mobileserver.model.UserProfile;
import com.painsomnia.mobileserver.persistence.UserRepository;
import com.painsomnia.mobileserver.service.UserService;
import org.hibernate.NonUniqueObjectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: Sergey Vasilenko
 * Created: 17.10.16.
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User get(String username) {
        return userRepository.findOne(username);
    }

    @Override
    public User create(User user, Authority... authorities) {
        final String username = user.getUsername();
        if (exists(username)) {
            throw new NonUniqueObjectException("User with the given username already exists", username, User.class.getName());
        }

        //just in case set bidirectional fields
        final UserProfile userProfile = user.getUserProfile();
        userProfile.setUsername(username);
        userProfile.setUser(user);


        user.setEnabled(true);
        final String rawPassword = user.getPassword();
        encodeUserPassword(user, rawPassword);
        user.addAuthority(Authority.USER);
        for (Authority authority : authorities) {
            user.addAuthority(authority);
        }
        return userRepository.save(user);
    }


    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public boolean exists(String username) {
        return userRepository.exists(username);
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Override
    public List<User> searchUsers(String searchTerm) {
        return userRepository.searchUsers("%" + searchTerm + "%");
    }

    private void encodeUserPassword(User user, String rawPassword) {
        final String encodedPassword = passwordEncoder.encode(rawPassword);
        user.setPassword(encodedPassword);
    }
}
