package com.painsomnia.mobileserver.bootstrap;

import com.painsomnia.mobileserver.constant.Authority;
import com.painsomnia.mobileserver.model.User;
import com.painsomnia.mobileserver.model.UserProfile;
import com.painsomnia.mobileserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Author: Sergey Vasilenko
 * Created: 17.10.16.
 */
@Component
public class AppBootstrap {

    public static final int INITIAL_USER_COUNT = 20;
    private final UserService userService;

    @Autowired
    public AppBootstrap(UserService userService) {
        this.userService = userService;
    }

    @PostConstruct
    public void init() {
        createAdmin();
        if (userService.count() < INITIAL_USER_COUNT) {
            createMultipleUsers();
        }
    }

    private void createAdmin() {
        if (!userService.exists(User.ADMIN)) {
            final User user = new User();
            user.setUsername(User.ADMIN);
            user.setEmailAddress("admin@admin.com");
            user.setPassword("admin");
            user.addAuthority(Authority.ADMIN);

            final UserProfile userProfile = new UserProfile();
            userProfile.setLastName(User.ADMIN);
            userProfile.setFirstName(User.ADMIN);
            userProfile.setPhoneNumber("123");
            userProfile.setCompany("company");
            user.setUserProfile(userProfile);
            userProfile.setUser(user);

            userService.create(user);
        }
    }

    private void createMultipleUsers() {
        for (int i = 0; i < INITIAL_USER_COUNT; i++) {
            createTestUser(i);
        }
    }

    private void createTestUser(int i) {
        final User user = new User();
        final String username = "user_" + i;
        user.setUsername(username);
        user.setEmailAddress(username + "@admin.com");
        user.setPassword("admin");

        final UserProfile userProfile = new UserProfile();
        userProfile.setFirstName("first_" + username);
        userProfile.setLastName("last_" + username);
        userProfile.setPhoneNumber("123");
        userProfile.setCompany("company");
        user.setUserProfile(userProfile);

        userService.create(user);
    }
}
