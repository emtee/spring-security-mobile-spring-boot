package com.painsomnia.mobileserver.constant;

/**
 * Author: Sergey Vasilenko
 * Created: 28.09.16.
 */
public class RepositoryQuery {
    public static final String SEARCH_BY_MASK_QUERY = "from User u " +
            "join u.userProfile p " +
            "where lower(u.username) like lower(:searchTerm) or " +
            "lower(p.firstName) like lower(:searchTerm) or " +
            "lower(p.lastName) like lower(:searchTerm) or " +
            "lower(u.emailAddress) like lower(:searchTerm)";

    private RepositoryQuery(){}
}
