package com.painsomnia.mobileserver.constant;

/**
 * Author: Sergey Vasilenko
 * Created: 20.09.16.
 */
public enum Authority {
    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER");

    private String name;

    Authority(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
