package com.painsomnia.mobileserver.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

/**
 * Author: Sergey Vasilenko
 * Created: 17.10.16.
 */
@Configuration
@Profile("dev")
public class PersistenceConfigDev {

    @Bean
    public DataSource dataSource() {
        return DataSourceBuilder
                .create()
                .url("jdbc:hsqldb:mem:mserver")
                .username("sa")
                .password("sa")
                .build();
    }
}
