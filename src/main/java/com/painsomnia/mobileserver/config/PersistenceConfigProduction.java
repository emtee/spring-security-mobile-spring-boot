package com.painsomnia.mobileserver.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

/**
 * Author: Sergey Vasilenko
 * Created: 17.10.16.
 */
@Configuration
@Profile("production")
public class PersistenceConfigProduction {

    @Bean
    public DataSource dataSource() {
        return DataSourceBuilder
                .create()
                .url("jdbc:hsqldb:file:${user.home}/.mobile-server/db/mserver")
                .username("sa")
                .password("sa")
                .build();
    }
}
