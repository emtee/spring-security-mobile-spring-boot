package com.painsomnia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityMobileApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityMobileApplication.class, args);
    }
}
