package com.painsomnia;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringSecurityMobileApplicationTests {

    @Autowired
    WebApplicationContext context;

    @Autowired
    FilterChainProxy filterChain;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = webAppContextSetup(this.context).addFilters(this.filterChain).build();
        SecurityContextHolder.clearContext();
    }

    @Test
    public void testUnauthorizedAccess() throws Exception {
        this.mvc.perform(get("/api/test/insecure").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andDo(print());

        this.mvc.perform(get("/api/test/secured").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized()).andDo(print());
    }

    @Test
    public void testSecuredAccess() throws Exception {
        String header = "Basic " + new String(Base64.encode("client-with-secret:secret".getBytes()));
        MvcResult result = this.mvc
                .perform(post("/oauth/token")
                        .header("Authorization", header)
                        .param("grant_type", "password")
                        .param("scope", "read")
                        .param("username", "admin")
                        .param("password", "admin"))
                .andExpect(status().isOk()).andDo(print()).andReturn();
        Object accessToken = this.objectMapper
                .readValue(result.getResponse().getContentAsString(), Map.class)
                .get("access_token");


        this.mvc.perform(get("/api/test/secured")
                .accept(MediaType.APPLICATION_XML)
                .header("Authorization", "Bearer " + accessToken))
                .andExpect(header().string("Content-Type",
                        MediaType.APPLICATION_XML_VALUE + ";charset=UTF-8"))
                .andExpect(status().isOk()).andDo(print()).andReturn();
    }

    @Test
    public void testRefreshToken() throws Exception {
        String header = "Basic " + new String(Base64.encode("client-with-secret:secret".getBytes()));
        MvcResult result = this.mvc
                .perform(post("/oauth/token")
                        .header("Authorization", header)
                        .param("grant_type", "password")
                        .param("scope", "read")
                        .param("username", "admin")
                        .param("password", "admin"))
                .andExpect(status().isOk()).andDo(print()).andReturn();
        Object refreshToken = this.objectMapper
                .readValue(result.getResponse().getContentAsString(), Map.class)
                .get("refresh_token");


        this.mvc
                .perform(post("/oauth/token")
                        .header("Authorization", header)
                        .param("grant_type", "refresh_token")
                        .param("refresh_token", (String) refreshToken))
                .andExpect(status().isOk()).andDo(print()).andReturn();
    }

}
